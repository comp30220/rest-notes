package controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import model.Note;

import java.util.Date;
import java.util.Map;
import java.util.List;
import java.util.LinkedList;
import java.util.TreeMap;

@RestController
public class NoteController {
    private static long SEED_ID = 0;

    private Map<Long, Note> notes = new TreeMap<Long, Note>();

    @GetMapping("/notes")
    public List<Note> getNotes() {
        List<Note> list = new LinkedList<>();
        for (Note note : notes.values()) {
            list.add(note);
        }
        return list;
    }

    @PostMapping("/notes")
    public Note createNote(@RequestBody Note note) {
        note.setId(SEED_ID++);
        note.setPublished(new Date(System.currentTimeMillis()));
        notes.put(note.getId(), note);
        return note;
    }

    @GetMapping("/notes/{id}")
    public Note getNote(@PathVariable("id") Long id) {
        Note note = notes.get(id);
        if (note == null) {
            throw new NoSuchNoteException();
        }
        return note;
    }

    @PutMapping("/notes/{id}")
    public Note updateNote(@PathVariable("id") Long id, @RequestBody Note newNote) {
        Note note = notes.get(id);
        if (note == null) {
            throw new NoSuchNoteException();
        }
        note.update(newNote);
        note.setPublished(new Date(System.currentTimeMillis()));
        return note;
    }

    @DeleteMapping("/notes/{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deleteNote(@PathVariable("id") Long id) {
        Note note = notes.remove(id);
        if (note == null) {
            throw new NoSuchNoteException();
        }
    }
}