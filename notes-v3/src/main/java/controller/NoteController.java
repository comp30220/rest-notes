package controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class NoteController {
    @Autowired
    NoteRepository repository;

    @GetMapping("/notes")
    public List<Note> getNotes() {
        return repository.findAll();
    }

    @PostMapping("/notes")
    public Note createNote(@RequestBody Note note) {
        return repository.save(note);
    }

    @GetMapping("/notes/{id}")
    public Note getNote(@PathVariable("id") Long id) {
        return repository.getOne(id);
    }

    @PutMapping("/notes/{id}")
    public Note updateNote(@PathVariable("id") Long id, @RequestBody Note newNote) {
        Note note = repository.getOne(id);
        if (note == null) {
            throw new NoSuchNoteException();
        }
        note.update(newNote);
        return repository.save(note);
    }

    @DeleteMapping("/notes/{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deleteNote(@PathVariable("id") Long id) {
        repository.deleteById(id);
    }
}