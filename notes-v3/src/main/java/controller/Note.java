package controller;

import java.util.Date;

import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Table(name="notes")
public class Note {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;

	@Column
	private String title;

	@Column
	private String author;

	@Column
	private String content;

	@Column(insertable=false,updatable=false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date published;

	public Date getPublished() {
		return published;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setPublished(Date published) {
		this.published = published;
    }
    
	public String getContent() {
		return content;
    }
    
	public void setContent(String content) {
		this.content = content;
    }
    
	public long getId() {
		return id;
    }
    
	public void setId(long id) {
		this.id = id;
	}

	public void update(Note newNote) {
		title = newNote.title;
		author = newNote.author;
		content = newNote.content;
	}
}