$(document).ready(reload_view());

function reload_view() {
    $.ajax({
        "type" : "GET",
        "url" : "/notes",
        "dataType" : "json",
        "success": function(data) {
            // Clear the notes
            $("#notes").empty();
            $.each(data, function(index,note) {
                $("#notes").append(
                    "<tr><td>"+note.title+"</td><td>"+note.content+"</td><td>"+note.author+"</td><td>"+note.published+"</td><td>"+
                    "<button class='btn btn-primary' onclick='removeNote("+note.id+")'>Delete</button>" +
                    "</td></tr>");
            });
        }
    });
}

function addNote() {
    BootstrapDialog.show({
        title: 'Add Note',
        message: $('<div></div>').load("add_notes.html"),
        buttons: [{
            label: 'Cancel',
            action: function(dialog) {
                dialog.close();
            }
        }, {
            label: 'OK',
            action: function(dialog) {
                note = {
                    "title" : $("#title").val(),
                    "author" : $("#author").val(),
                    "content" : $("#content").val()
                };

                $.ajax({
                    "type" : "POST",
                    "url" : "/notes",
                    "contentType": "application/json",
                    "data" : JSON.stringify(note),
                    "dataType" : "json",
                    "success" : function(data) {
                        reload_view();
                    }
                });

                dialog.close();
            }
        }]
    });
}

function removeNote(id) {
    $.ajax({
        "type" : "DELETE",
        "url" : "/notes/"+id,
        "dataType" : "json",
        "success" : function(data) {
            reload_view();
        }
    });   
}