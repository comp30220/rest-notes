package model;

import java.util.Date;

public class Note {
	private long id;
	private String title;
	private String author;
	private String content;
	private Date published;

	public Date getPublished() {
		return published;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setPublished(Date published) {
		this.published = published;
    }
    
	public String getContent() {
		return content;
    }
    
	public void setContent(String content) {
		this.content = content;
    }
    
	public long getId() {
		return id;
    }
    
	public void setId(long id) {
		this.id = id;
	}

	public void update(Note newNote) {
		title = newNote.title;
		author = newNote.author;
		content = newNote.content;
	}
}